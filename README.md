# evergy2mqtt

**The full updated project documentation can be found at [https://hydroqc.ca](https://hydroqc.ca)**

This module extracts data from your Evergy account using the [Evergy API Wrapper](https://gitlab.com/jruby411/evergy) and will post it to your MQTT server. Home-Assistant MQTT Discovery topics are also provided, automating the creation of your sensors in Home-Assistant.

We have implemented a feature that will send historical hourly consumption from Hydro-Quebec to your Home-Assistant statistics to be used in the Energy Dashboard. This feature does not work over MQTT but send the information directly to Home-Assistant via Websocket.

## Disclaimer

### **Not an official Evergy API**

This is a non official way to extract your data from Evergy, while it works now it may break at anytime if or when Evergy change their systems.
