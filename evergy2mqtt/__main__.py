"""Module defining entrypoint."""
import argparse
import asyncio
import os

from evergy2mqtt.daemon import Evergy2Mqtt


def _parse_cmd() -> argparse.Namespace:
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        prog="evergy2mqtt", description="evergy2mqtt daemon"
    )
    parser.add_argument(
        "--mqtt-host",
        required=False,
        default=None,
        help="Mqtt host address. Default: 127.0.0.1",
    )
    parser.add_argument(
        "--mqtt-port",
        required=False,
        default=None,
        type=int,
        help="Mqtt host port. Default: 1883",
    )
    parser.add_argument(
        "--mqtt-username", required=False, default=None, help="Mqtt username"
    )
    parser.add_argument(
        "--mqtt-password", required=False, default=None, help="Mqtt password"
    )
    parser.add_argument(
        "--mqtt-discovery-root-topic",
        required=False,
        default="homeassistant",
        help="Mqtt root topic for Home Assistant discovery. Default: homeassistant",
    )
    parser.add_argument(
        "--mqtt-data-root-topic",
        required=False,
        default="evergy",
        help="Mqtt root topic for sensors data. Default: homeassistant",
    )
    parser.add_argument("--log-level", required=False, default="info", help="Log level")
    parser.add_argument(
        "--http-log-level", required=False, default="warning", help="HTTP Log level"
    )
    parser.add_argument(
        "--config",
        required=False,
        default=None,
        help="Config file path. Default: config.yaml",
    )
    parser.add_argument(
        "--ev-username", required=False, default=None, help="Evergy username"
    )
    parser.add_argument(
        "--ev-password", required=False, default=None, help="Evergy password"
    )
    parser.add_argument(
        "--ev-name",
        required=False,
        default=None,
        help="Name of your account. Used for prefixing data. Default: myhouse",
    )
    parser.add_argument(
        "--ev-customer-id", required=False, default=None, help="Evergy customer ID"
    )
    parser.add_argument(
        "--ev-account-id", required=False, default=None, help="Evergy account ID"
    )
    parser.add_argument(
        "--ev-premise-id", required=False, default=None, help="Evergy premise ID"
    )
    parser.add_argument(
        "--run-once",
        required=False,
        default=False,
        action="store_true",
        help="Run once and exit. Useful to run as cronjob.",
    )

    cmd_args = parser.parse_args()
    return cmd_args


def main() -> None:
    """Entrypoint function."""
    cmd_args = _parse_cmd()

    dev = Evergy2Mqtt(
        mqtt_host=cmd_args.mqtt_host,
        mqtt_port=cmd_args.mqtt_port,
        mqtt_username=cmd_args.mqtt_username,
        mqtt_password=cmd_args.mqtt_password,
        mqtt_discovery_root_topic=cmd_args.mqtt_discovery_root_topic,
        mqtt_data_root_topic=cmd_args.mqtt_data_root_topic,
        config_file=cmd_args.config,
        run_once=cmd_args.run_once,
        log_level=os.getenv("EV2M_CONTRACTS_0_LOG_LEVEL", cmd_args.log_level),
        http_log_level=cmd_args.http_log_level,
        ev_username=cmd_args.ev_username,
        ev_password=cmd_args.ev_password,
        ev_name=cmd_args.ev_name,
        ev_customer_id=cmd_args.ev_customer_id,
        ev_account_id=cmd_args.ev_account_id,
        ev_premise_id=cmd_args.ev_premise_id,
    )
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
