"""Sensor definition list."""
from typing import TypedDict

# TODO: python 3.11 => uncomment Required
# from typing_extensions import Required


# TODO: python 3.11 => uncomment Required
class SensorType(TypedDict, total=False):
    """Sensor entity settings dict format."""

    name: str
    data_source: str
    device_class: str | None
    expire_after: int
    force_update: bool
    entity_category: str | None
    # device_class: Required[str | None]
    # expire_after: Required[int]
    # force_update: Required[bool]
    icon: str | None
    state_class: str | None
    unit: str | None
    sub_mqtt_topic: str
    object_id: str


class BinarySensorType(TypedDict, total=False):
    """Binary sensor entity settings dict format."""

    name: str
    data_source: str
    device_class: str | None
    expire_after: int
    force_update: bool
    icon: str
    sub_mqtt_topic: str
    object_id: str


HOURLY_CONSUMPTION_HISTORY_SWITCH = {
    "name": "Sync hourly consumption history",
    "icon": "mdi:clipboard-text-clock",
    "entity_category": "config",
    "sub_mqtt_topic": "premise/state",
    "optimistic": False,
}

HOURLY_CONSUMPTION_HISTORY_DAYS = {
    "name": "Days to sync in hourly consumption history",
    "icon": "mdi:calendar-clock",
    "entity_category": "config",
    "sub_mqtt_topic": "premise/state",
    "min_value": 0,
    "max_value": 800,
    "mode": "auto",
    "unit": "days",
    "step": 1,
    "optimistic": True,
    "start_value": 731,
}

HOURLY_CONSUMPTION_CLEAR_BUTTON = {
    "name": "Clear hourly consumption history",
    "icon": "mdi:broom",
    "entity_category": "config",
    "sub_mqtt_topic": "premise/state",
}


HOURLY_CONSUMPTION_SENSOR: SensorType = {
    "name": "Hourly consumption",
    "device_class": "energy",
    "expire_after": 0,
    "entity_category": "diagnostic",
    "force_update": False,
    "icon": "mdi:lightning-bolt",
    "state_class": "total",
    "unit": "kWh",
    "sub_mqtt_topic": "premise/state",
}


SENSORS: dict[
    str, SensorType
] = {  # pylint: disable=consider-using-namedtuple-or-dataclass
    # Account
    "balance": {
        "name": "Balance",
        "data_source": "account.balance",
        "device_class": "monetary",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:currency-usd",
        "state_class": "measurement",
        "unit": "USD",
        "sub_mqtt_topic": "account/state",
    },
    # Premise
    "current_billing_period_current_day": {
        "name": "Current billing period current day",
        "data_source": "premise.cp_current_day",
        "device_class": None,
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:calendar-start",
        "state_class": "measurement",
        "unit": "days",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_duration": {
        "name": "Current billing period duration",
        "data_source": "premise.cp_duration",
        "device_class": None,
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:calendar-expand-horizontal",
        "state_class": "measurement",
        "unit": "days",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_total_to_date": {
        "name": "Current billing period total to date",
        "data_source": "premise.cp_current_bill",
        "device_class": "monetary",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:currency-usd",
        "state_class": "total_increasing",
        "unit": "USD",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_projected_bill": {
        "name": "Current billing period projected bill",
        "data_source": "premise.cp_projected_bill",
        "device_class": "monetary",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:currency-usd",
        "state_class": "measurement",
        "unit": "USD",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_daily_bill_mean": {
        "name": "Current billing period daily bill mean",
        "data_source": "premise.cp_daily_bill_mean",
        "device_class": "monetary",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:currency-usd",
        "state_class": "measurement",
        "unit": "USD",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_daily_consumption_mean": {
        "name": "Current billing period daily consumption mean",
        "data_source": "premise.cp_daily_consumption_mean",
        "device_class": "energy",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:home-lightning-bolt",
        "state_class": "measurement",
        "unit": "kWh",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_total_consumption": {
        "name": "Current billing period total consumption",
        "data_source": "premise.cp_total_consumption",
        "device_class": "energy",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:home-lightning-bolt",
        "state_class": "total_increasing",
        "unit": "kWh",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_projected_total_consumption": {
        "name": "Current billing period projected total consumption",
        "data_source": "premise.cp_projected_total_consumption",
        "device_class": "energy",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:home-lightning-bolt",
        "state_class": "measurement",
        "unit": "kWh",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_higher_price_consumption": {
        "name": "Current billing period higher price consumption",
        "data_source": "premise.cp_higher_price_consumption",
        "device_class": "energy",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:home-lightning-bolt",
        "state_class": "measurement",
        "unit": "kWh",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_lower_price_consumption": {
        "name": "Current billing period lower price consumption",
        "data_source": "premise.cp_lower_price_consumption",
        "device_class": "energy",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:home-lightning-bolt-outline",
        "state_class": "measurement",
        "unit": "kWh",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_average_temperature": {
        "name": "Current billing period average temperature",
        "data_source": "premise.cp_average_temperature",
        "device_class": "temperature",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:thermometer",
        "state_class": "measurement",
        "unit": "°F",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_kwh_cost_mean": {
        "name": "Current billing period kwh cost mean",
        "data_source": "premise.cp_kwh_cost_mean",
        "device_class": "monetary",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:currency-usd",
        "state_class": "measurement",
        "unit": "USD",
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_rate": {
        "name": "Current billing period rate",
        "data_source": "premise.rate",
        "device_class": None,
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:playlist-check",
        "state_class": None,
        "unit": None,
        "sub_mqtt_topic": "premise/state",
    },
    "current_billing_period_rate_option": {
        "name": "Current billing period rate option",
        "data_source": "premise.rate_option",
        "device_class": None,
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:playlist-star",
        "state_class": None,
        "unit": None,
        "sub_mqtt_topic": "premise/state",
    },
    # Peak Demand
    "daily_peak_demand_time": {
        "name": "Daily peak demand time",
        "data_source": "premise.cp_current_day_peak_time",
        "device_class": "timestamp",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:clock-start",
        "sub_mqtt_topic": "premise/state",
    },
    "daily_peak_demand": {
        "name": "Daily peak demand",
        "data_source": "premise.cp_current_day_peak",
        "device_class": "power",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:home-lightning-bolt",
        "state_class": "measurement",
        "unit": "kW",
        "sub_mqtt_topic": "premise/state",
    },
}
BINARY_SENSORS: dict[
    str, BinarySensorType
] = {  # pylint: disable=consider-using-namedtuple-or-dataclass
    # Premises
    "current_period_epp_enabled": {
        "name": "Current period epp enabled",
        "data_source": "premise.cp_epp_enabled",
        "expire_after": 0,
        "force_update": False,
        "icon": "mdi:code-equal",
        "sub_mqtt_topic": "premise/state",
    },
}
