"""Evergy2Mqtt Error Module."""


class Evergy2MqttError(Exception):
    """Base Evergy2Mqtt Error."""


class Evergy2MqttWSError(Exception):
    """Base Evergy2Mqtt WebSocket Error."""
