"""Mqtt Daemon module."""
import asyncio
import os
import re
import sys
from contextlib import AsyncExitStack
from typing import Any, TypedDict

import asyncio_mqtt as mqtt
import evergy
import yaml
from mqtt_hass_base.daemon import MqttClientDaemon
from mqtt_hass_base.error import MQTTHassBaseError

from evergy2mqtt.premise_device import (
    EvergyPremiseConfigType,
    EvergyPremiseDevice,
)
from evergy2mqtt.error import Evergy2MqttError, Evergy2MqttWSError

# TODO: python 3.11 => uncomment NotRequired
# from typing_extensions import NotRequired


MAIN_LOOP_WAIT_TIME = 600
OVERRIDE_REGEX = re.compile(
    # TODO add env
    (
        r"^HQ2M_CONTRACTS_(\d*)_("
        "USERNAME|"
        "PASSWORD|"
        "CUSTOMER|"
        "ACCOUNT|"
        "CONTRACT|"
        "NAME|"
        "LOG_LEVEL|"
        "HTTP_LOG_LEVEL|"
        "SYNC_HOURLY_CONSUMPTION_ENABLED|"
        "HOURLY_CONSUMPTION_SENSOR_NAME|"
        "SYNC_HOURLY_CONSUMPTION_HISTORY_ENABLED|"
        "HOME_ASSISTANT_WEBSOCKET_URL|"
        "HOME_ASSISTANT_TOKEN)$"
    )
)


# TODO: python 3.11 => remove total and uncomment NotRequired
class ConfigType(TypedDict, total=False):
    """Binary sensor entity settings dict format."""

    # sync_frequency: notrequired[int]
    # unregister_on_stop: notrequired[bool]
    sync_frequency: int
    unregister_on_stop: bool
    premises: list[EvergyPremiseConfigType]


class Evergy2Mqtt(MqttClientDaemon):
    """MQTT Sensor Feed."""

    def __init__(
        self,
        mqtt_host: str,
        mqtt_port: int,
        mqtt_username: str,
        mqtt_password: str,
        mqtt_discovery_root_topic: str,
        mqtt_data_root_topic: str,
        config_file: str,
        run_once: bool,
        log_level: str,
        http_log_level: str,
        hq_username: str,
        hq_password: str,
        hq_name: str,
        hq_customer_id: str,
        hq_account_id: str,
        hq_premise_id: str,
    ):  # pylint: disable=too-many-arguments
        """Create a new MQTT Evergy Sensor object."""
        self.premises: list[EvergyPremiseDevice] = []
        self.config_file = config_file
        self._run_once = run_once
        self._hq_username = hq_username
        self._hq_password = hq_password
        self._hq_name = hq_name
        self._hq_customer_id = hq_customer_id
        self._hq_account_id = hq_account_id
        self._hq_premise_id = hq_premise_id
        self._connected = False
        self._http_log_level = http_log_level
        self._needs_mqtt_reconnection: bool = False
        self.config: ConfigType = {}

        MqttClientDaemon.__init__(
            self,
            "evergy2mqtt",
            mqtt_host,
            mqtt_port,
            mqtt_username,
            mqtt_password,
            mqtt_discovery_root_topic,
            mqtt_data_root_topic,
            log_level,
        )

    def read_config(self) -> None:
        """Read env vars."""
        if self.config_file is None:
            self.config_file = os.environ.get("CONFIG_YAML", "config.yaml")
        if os.path.exists(self.config_file):
            with open(self.config_file, "rb") as fhc:
                self.config = yaml.safe_load(fhc)
        self.config.setdefault("premises", [])

        # Override evergy settings from env var if exists over config file
        config: dict[str, Any] = {}
        config["premises"] = self.config["premises"]
        # if config["premises"] is None:
        #    config["premises"] = []

        # TODO we should ensure that  os.environ.items() are sorted abc...

        for env_var, value in os.environ.items():
            if env_var == "HQ2M_SYNC_FREQUENCY":
                self.config["sync_frequency"] = int(value)
                continue
            match_res = OVERRIDE_REGEX.match(env_var)
            if match_res and len(match_res.groups()) == 2:
                index = int(match_res.group(1))
                # username|password|customer|account|premise|name
                kind = match_res.group(2).lower()
                # TODO improve me
                try:
                    # Check if the premises is set in the config file
                    config["premises"][index]
                except IndexError:
                    config["premises"].append({})
                if env_var.endswith("_ENABLED"):
                    # Handle boolean values
                    config["premises"][index][kind[: -len("_ENABLED")]] = (
                        value.lower() == "true"
                    )
                else:
                    config["premises"][index][kind] = value
        if "http_log_level" not in config["premises"][0] and self._http_log_level:
            config["premises"][0]["http_log_level"] = self._http_log_level

        # Override evergy settings
        if self._hq_username:
            config["premises"][0]["username"] = self._hq_username
        if self._hq_password:
            config["premises"][0]["password"] = self._hq_password
        if self._hq_name:
            config["premises"][0]["name"] = self._hq_name
        if self._hq_customer_id:
            # Should be customer ?
            config["premises"][0]["customer_id"] = self._hq_customer_id
        if self._hq_account_id:
            config["premises"][0]["account_id"] = self._hq_account_id
        if self._hq_premise_id:
            config["premises"][0]["premise_id"] = self._hq_premise_id

        self.config["premises"] = config["premises"]
        self.sync_frequency = int(
            self.config.get("sync_frequency", MAIN_LOOP_WAIT_TIME)
        )

        self.unregister_on_stop = bool(self.config.get("unregister_on_stop", False))

    async def _init_main_loop(self, stack: AsyncExitStack) -> None:
        """Init before starting main loop."""
        # Handle premises
        for premise_config in self.config["premises"]:
            premise = EvergyPremiseDevice(
                premise_config["name"],
                self.logger,
                premise_config,
                self.mqtt_discovery_root_topic,
                self.mqtt_data_root_topic,
                self.mqtt_client,
            )

            self._connected = await premise.init_session()
            if not self._connected:
                self.logger.fatal(
                    "Can not start because we can not login at the startup."
                )
                sys.exit(1)

            await premise.add_entities()
            self.premises.append(premise)

            # Register premise's entities to mqtt
            await premise.register()
            # Subscribes
            await premise.subscribe(self.tasks, stack)

    async def _main_loop(self, stack: AsyncExitStack) -> None:
        """Run main loop."""
        try:
            # Handle reconnection needed
            if self._needs_mqtt_reconnection:
                self.logger.info("Mqtt trying to reconnect")
                await self._mqtt_connect(stack)
                self.logger.info("Reinit premises objects")
                for premise in self.premises:
                    premise.set_mqtt_client(self.mqtt_client)
                self._needs_mqtt_reconnection = False

            # Connect to premises
            for premise in self.premises:
                await premise.init_session()

            # Get premise data
            for premise in self.premises:
                await premise.update()

            # Sync_consumption_statistics
            for premise in self.premises:
                if (
                    premise.hourly_consumption_sync_enabled
                    and not premise.is_consumption_history_syncing
                ):
                    await premise.sync_consumption_statistics()

        except evergy.error.EvergyHTTPError as exp:
            self.logger.error("E0010: Evergy lib error: %s", exp)
        except Evergy2MqttWSError as exp:
            self.logger.error(exp)
        except Evergy2MqttError as exp:
            self.logger.error(exp)
        except (mqtt.MqttError, MQTTHassBaseError) as exp:
            self.logger.error("E0011: %s", exp)
            # Reconnect to Mqtt
            self._needs_mqtt_reconnection = True
            self.logger.warning("We will try to reconnect to MQTT server.")

        if self._run_once:
            self.must_run = False
            return

        i = 0
        while i < self.sync_frequency and self.must_run:
            await asyncio.sleep(1)
            i += 1

    async def _loop_stopped(self) -> None:
        """Run after the end of the main loop."""
        for premise in self.premises:
            await premise.close()

    async def _signal_handler(self, sig_name: str) -> None:
        """Handle SIGKILL."""
        if self.unregister_on_stop:
            for premise in self.premises:
                await premise.unregister()

    async def _on_disconnect(
        self,
    ) -> None:
        """MQTT on disconnect callback."""
