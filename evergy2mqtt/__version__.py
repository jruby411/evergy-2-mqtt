"""Evergy2mqtt version."""
from importlib.metadata import PackageNotFoundError, version

try:
    VERSION = version("evergy2mqtt")
except PackageNotFoundError:
    # package is not installed
    print("Python package `evergy2mqtt` is not installed. Install it using pip")
